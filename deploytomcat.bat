
:: GIT PULL ########################################
git pull https://gitlab.com/yoan1601/deploylearning.git main

:: COMPILATION #####################################
:: Spécifiez les répertoires source et destination
set source_dir=src\DeployLearning\src
set dev_web=..\dev_deploylearning\web
set destination_dir=%dev_web%\WEB-INF\classes

:: Initialisez la variable 'jar_file'
set jar_file=

:: Répertoire contenant les fichiers jar
set jar_dir=src\DeployLearning\lib

:: Parcourez chaque fichier jar dans le répertoire
for %%i in (%jar_dir%\*.jar) do (
    :: Vérifiez si 'jar_file' est vide
    if not defined jar_file (
        :: Si 'jar_file' est vide, ajoutez simplement le chemin du fichier
        set jar_file=%%i
    ) else (
        :: Sinon, ajoutez un point-virgule et le chemin du fichier
        set jar_file=!jar_file!;%%i
    )
)

:: Trouvez tous les fichiers Java de manière récursive et écrivez leurs chemins dans un fichier texte
dir /b /s /a-d %source_dir%\*.java > java_files.txt

:: Compilez tous les fichiers Java en utilisant 'javac' avec l'option de liste de fichiers et le chemin de classe
javac -cp "%jar_file%" @java_files.txt -d "%destination_dir%"

:: Nettoyage : supprimez le fichier temporaire
del java_files.txt

:: DEPLOIEMENT #####################################
cd %dev_web%
jar -cvf F:/apache-tomcat-10/webapps/deploylearning.war *

cd ..
cmd
